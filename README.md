# NationBuilder email statistics
This is a simple ruby script to allow for scraping of the Nation Builder mailings statistics page and saving
to an Excel spreadsheet.

# Installation instructions

## 1. Requirements
This script uses bundler to manage its dependencies. If you do not have bundler installed, please install
it by running either `gem install bundler` or `sudo gem install bundler`.

## 2. Install Gems
Once you have bundler installed run `bundle install` to install the required gems.

## 3. Configuration
Copy the `config.rb.example` file to `config.rb` and edit the `config.rb` file to include your NationBuilder
email address and password as well as the link to your broadcaster's mailing page.

## 4. Running
Simply run `ruby email_stats.rb` and it will save the Excel spreadsheet to the same folder you are in.

Optionally you can add a number of pages that you want the command to scrape.

_Enjoy!_