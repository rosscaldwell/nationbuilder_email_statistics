require 'rubygems'
require 'bundler'
Bundler.require(:default, :ci)

require './config'

if ARGV[0].to_i == 0
	@times = 1
else
	@times = ARGV[0].to_i
end

m = Mechanize.new
puts "Loading #{URL}"
page = m.get(URL)

# Fill out the login form
puts "Processing authentication..."
login_form = page.forms[1]
login_form['user_session[email]'] = USERNAME
login_form['user_session[password]'] = PASSWORD

# Get our new page
@mailings_page = login_form.submit
puts "Successfully loaded mailings page..."

# Create an array for our rows
@data = []

# Download the table headings
th = @mailings_page.search('table.data-table tr')[0] 
	local_data = []
		th.search("th").each do |td|
			if td.text != '\n' or td.text != '  '
				text = td.text.gsub("\n", " ").strip
				local_data << text unless text == ""
			end
		end
	@data << local_data

def load_rows(mailings_page)
	mailings_page.search('table.data-table tr').each do |tr|
		local_data = []
			tr.search("td").each do |td|
				if td.text != '\n' or td.text != '  '
					text = td.text.gsub("\n", " ").strip
					local_data << text unless text == ""
				end
			end
		@data << local_data unless local_data == []
	end
end

# Download the mailing data
@times.times do |i|
	if i == 0
		load_rows @mailings_page
	else
		@mailings_page = @mailings_page.link_with(href: /page=/).click
		load_rows @mailings_page
	end
end


# Now let's export to a spreadsheet
p = Axlsx::Package.new
workbook = p.workbook

workbook.add_worksheet(:name => "Mail Stats") do |sheet|
	currency = workbook.styles.add_style :num_fmt => 5
	normal = workbook.styles.add_style
	date = workbook.styles.add_style :format_code => "yyyy-mm-dd hh:mm"

	sheet.add_row @data[0]

	@data.drop(1).each do |d|
		d[5] = d[5].sub("$","").to_f
		sheet.add_row d, :style => [normal, normal, normal, normal, normal, currency, normal, normal, normal, normal, normal, date]
	end

end

puts "Saving spreadsheet to #{SAVE_TO}"
p.serialize(SAVE_TO)